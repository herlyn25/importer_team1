from django.contrib import admin

# Register your models here.
from django.db import models

from analyzerfiles.models import Document, FileExcel


@admin.register(Document)
class Document(admin.ModelAdmin):
    list_display = ['title','url','date_create']

@admin.register(FileExcel)
class FileExcelAdmin(admin.ModelAdmin):
    pass