from django.shortcuts import render

# Create your views here.
from django.urls import path

from importer_team1.views import create_document, list_content
urlpatterns = [
    path('', create_document,name='create_document'),
    path('list/<int:id_request>',list_content,name='list_content'),
]