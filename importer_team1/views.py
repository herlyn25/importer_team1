import pathlib

import pandas as pd
from django.shortcuts import render
from django.conf import settings

from testimporter.models import Document


def create_document(request):
    titlepage = 'Add Document'
    documents = None
    msg = ''

    if request.method == 'POST':
        ext = ['.csv', '.xlsx']
        file_title = request.POST['tittle']
        upload_file = request.FILES['file']
        extension = pathlib.Path(upload_file.name).suffix

        if extension in ext:
            print('entrp aqui')
            document = Document(title=file_title, url=upload_file)
            document.save()
            documents = Document.objects.all()
        else:
            print('entro aca al error')
            print(extension)
            msg = 'File no valid'

    context = {'title': titlepage, 'documents': documents, 'msg': msg}
    return render(request, 'create_document.html', context)


def list_content(request, id_request):
    title = 'List Documents'
    documents = Document.objects.get(pk=id_request)
    name_file = str(documents.url)
    path=f'{settings.MEDIA_ROOT}/{name_file}'
    ext = name_file[len(name_file) - 4:len(name_file)]
    df_list = []
    if ext == '.csv':
        try:
            df = pd.read_csv(path)
            columns_dict = [x for x in df.columns]
            for i in df.index.values:
                df_line = df.loc[i, columns_dict].to_dict()
                df_list.append(df_line)
        except FileNotFoundError:
            print(path)
    elif ext == 'xlsx':
        print('esta leyendo el excel')
        print(path)
        try:
            df = pd.read_excel(path)
            df.fillna("",inplace=True)
            columns_dict = [x for x in df.columns]
            for i in df.index.values:
                df_line = df.loc[i, columns_dict].to_dict()
                df_list.append(df_line)
        except FileNotFoundError:
            print('No se encontro el archivo')
            print(path)
    else:
        print("Ingreso al error")
        print(ext)
    print(df_list)
    context = {'title': title, 'documents': df_list}
    return render(request, 'list_content.html', context)
